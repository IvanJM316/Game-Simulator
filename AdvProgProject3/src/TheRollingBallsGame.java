import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

/**
 * Main class and launcher. Developed by Ivan J. Miranda.
 */
public class TheRollingBallsGame 
{
	public static JFrame gameWindow;
	public static int gameWindowX = 200;
	public static int gameWindowY = 50;
	public static int gameWindowWidth = 1000;
	public static int gameWindowHeight = 750;
	public static SimpleDateFormat dateOfGameFormat = new SimpleDateFormat("YYYY/MM/dd-HH:mm:ss"); //SimpleDateFormat for String representation of date. See SimpleDateFormat java class.
	public static final String GAME_TITLE = "The Rolling-Balls Game Simulator"; 
	public static final String DEV_NAME = "Ivan J. Miranda"; 
	public static final String VERSION = "1.0";
	
	public static void main(String[] args) 
	{
		new TheRollingBallsGame().launch(); //Launches game.
	}
	
	/**
	 * Launches the game.
	 */
	private void launch()
	{
		System.out.println(dateOfGameFormat.format(Calendar.getInstance().getTime())); //TODO Time stamp of game execution. My way of retrieving and
		//storing dates.
		
		File playerListFile = new File("PlayerList.txt"); //File containing registered users.	
		ArrayList<Player> playerList = PlayerManager.readPlayersFromFile(playerListFile);
		if(playerList == null) //File didn't exist and could not be created.
		{
			JOptionPane.showMessageDialog(null, playerListFile.getPath() + " file in " + playerListFile.getAbsolutePath() + " could not be created. "
					+ "Please refer to README.txt troubleshooting section for instructions "
					+ "in order to create the file manually.", "Fatal Error", JOptionPane.ERROR_MESSAGE);
			System.exit(1); 
		}
		
		File gamesHistoryFile = new File("GamesHistory.txt"); //File containing registered users.	
		ArrayList<ScoreEntry> gamesHistory = ScoreEntryManager.readHistoryFromFile(gamesHistoryFile);
		if(gamesHistory == null) //File didn't exist and could not be created.
		{
			JOptionPane.showMessageDialog(null, gamesHistoryFile.getPath() + " file in " + playerListFile.getAbsolutePath() + " could not be created. "
					+ "Please refer to README.txt troubleshooting section for instructions "
					+ "in order to create the file manually.", "Fatal Error", JOptionPane.ERROR_MESSAGE);
			System.exit(2); 
		}
		//CRITICAL FILES LOADED SUCCESFULLY.
		
		//CREATING FRAME.
		gameWindow = new JFrame(GAME_TITLE + " (by " + DEV_NAME + ")"); 
		gameWindow.setLocation(gameWindowX, gameWindowY);
		gameWindow.setSize(gameWindowWidth, gameWindowHeight);
		TitleScreen rollingBallsTitleScreen = new TitleScreen(); //Creates title screen window's graphics.
		gameWindow.getContentPane().add(rollingBallsTitleScreen); //Adds title screen to frame.
			
		/////////////////////////////////////////////////////////
		
		JMenuBar menuBar = new JMenuBar(); //Creating menu bar.
		
		/////////////////////////////////////////////////////////
		
		//New game option where you will either create a user, select from the existing ones or start a game.
		JMenu newGameMenu = new JMenu("New Game"); 
		newGameMenu.setToolTipText("Start a new game");
		
		JMenuItem firstTimePlayerOption = new JMenuItem("First Time Player"); //Option to create a user.
		firstTimePlayerOption.setToolTipText("Creates a new player");
		firstTimePlayerOption.addActionListener
		(
			new ActionListener() 
			{
				public void actionPerformed(ActionEvent ae) 
				{
					PlayerManager.createPlayerDialog(playerList, playerListFile);
				}
			}
		);
		newGameMenu.add(firstTimePlayerOption);
		
		newGameMenu.addSeparator();
		
		JMenuItem selectPlayer1Option = new JMenuItem("Select Player 1"); //Option to select player 1 from existing players.
		selectPlayer1Option.setToolTipText("Select from existing players");
		selectPlayer1Option.addActionListener
		(
			new ActionListener() 
			{
				public void actionPerformed(ActionEvent ae) 
				{
					PlayerManager.selectPlayerDialog(playerList, playerListFile, PlayerManager.PLAYER_ONE);;
				}

			}
		);
		newGameMenu.add(selectPlayer1Option);
		
		JMenuItem selectPlayer2Option = new JMenuItem("Select Player 2"); //Option to select player 2 from existing players.
		selectPlayer2Option.setToolTipText("Select from existing players");
		selectPlayer2Option.addActionListener
		(
			new ActionListener() 
			{
				public void actionPerformed(ActionEvent ae) 
				{
					PlayerManager.selectPlayerDialog(playerList, playerListFile, PlayerManager.PLAYER_TWO);
				}
			}
		);
		newGameMenu.add(selectPlayer2Option);
		
		newGameMenu.addSeparator();
		
		JMenuItem clearPlayerSelectionsOption = new JMenuItem("Clear Selections"); //Option to clear players selected.
		clearPlayerSelectionsOption.setToolTipText("Clear player selections");
		clearPlayerSelectionsOption.addActionListener
		(
			new ActionListener() 
			{
				public void actionPerformed(ActionEvent ae) 
				{
					PlayerManager.clearPlayerSelections();
				}
			}
		);
		newGameMenu.add(clearPlayerSelectionsOption);
		
		newGameMenu.addSeparator();
		
		JMenuItem startNewGameOption = new JMenuItem("Simulate Game"); //Option to start a new game with current players. (Simulating in the mean time)
		startNewGameOption.setToolTipText("Simulates game with current players");
		startNewGameOption.addActionListener
		(
			new ActionListener() 
			{
				public void actionPerformed(ActionEvent ae) 
				{
					//IF GAME SUMLATION IS SELECTED WITHOUT PLAYERS BEING SELECTED.
					if(PlayerManager.currentPlayer1 == null || PlayerManager.currentPlayer2 == null)
					{
						JOptionPane.showMessageDialog(null, "Please select both player 1 and player 2. The game will now take you to the "
								+ "selection dialog.", "Error", JOptionPane.ERROR_MESSAGE);
						PlayerManager.selectPlayerDialog(playerList, playerListFile, PlayerManager.PLAYER_TWO);
						PlayerManager.selectPlayerDialog(playerList, playerListFile, PlayerManager.PLAYER_ONE);
					}
					else
					{
						//GAME IS SIMULATED AND STORED IMMEDIATELY ALONG WITH ANY CHANGES IN HIGH SCORES AND GAME HISTORY ENTRIES.
						ScoreEntryManager.simulateGame(gamesHistory, PlayerManager.currentPlayer1, PlayerManager.currentPlayer2, dateOfGameFormat);
						try {
							PlayerManager.writeToFile(playerList, playerListFile);
							ScoreEntryManager.writeToFile(gamesHistory, gamesHistoryFile);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		);
		newGameMenu.add(startNewGameOption);
		
		menuBar.add(newGameMenu);
		
		//////////////////////////////////////////////////////////		
		
		JMenu historyMenu = new JMenu("History"); //Option to consult game history.
		historyMenu.setToolTipText("View played games' history");
		
		JMenuItem highScoreOption = new JMenuItem("Highest Winners Scores"); //Option to sort games played by score (highest first).
		highScoreOption.setToolTipText("Sort by winners high scores");
		highScoreOption.addActionListener
		(
			new ActionListener() 
			{
				public void actionPerformed(ActionEvent ae) 
				{
					ScoreEntryManager.showHistoryBy(gamesHistory, new ScoreEntryWinnerScoreComparator());
				}
			}
		);
		historyMenu.add(highScoreOption); 
		
		JMenuItem datePlayedOption = new JMenuItem("Date Played");  //Option to sort games played by date (latest game played first).
		datePlayedOption.setToolTipText("Latest games first");
		datePlayedOption.addActionListener
		(
			new ActionListener() 
			{
				public void actionPerformed(ActionEvent ae) 
				{
					ScoreEntryManager.showHistoryBy(gamesHistory, new ScoreEntryDateComparator());
				}
			}
		);
		historyMenu.add(datePlayedOption); 
		
		JMenuItem byNameOption = new JMenuItem("By Winners Names");  //Option to sort games played by name.
		byNameOption.setToolTipText("Sort lexicographically");
		byNameOption.addActionListener
		(
			new ActionListener() 
			{
				public void actionPerformed(ActionEvent ae) 
				{
					ScoreEntryManager.showHistoryBy(gamesHistory, new ScoreEntryWinnerNameComparator());
				}
			}
		);
		historyMenu.add(byNameOption); 
		
		historyMenu.addSeparator(); 
		
		JMenuItem registeredPlayersOption = new JMenuItem("Registered Players"); //Option to see registered players (ordered by name).
		registeredPlayersOption.setToolTipText("Alphabetically ordered");
		registeredPlayersOption.addActionListener
		(
			new ActionListener() 
			{
				public void actionPerformed(ActionEvent ae) 
				{
					PlayerManager.showPlayersBy(playerList, new PlayerListNameComparator());
				}
			}
		);
		historyMenu.add(registeredPlayersOption);
		
		JMenuItem registeredPlayersByHighScoreOption = new JMenuItem("Registered Players (by High Score)"); //Option to see registered players (ordered by name).
		registeredPlayersByHighScoreOption.setToolTipText("Sorted by highest scores");
		registeredPlayersByHighScoreOption.addActionListener
		(
			new ActionListener() 
			{
				public void actionPerformed(ActionEvent ae) 
				{
					PlayerManager.showPlayersBy(playerList, new PlayerListScoreComparator());
				}
			}
		);
		historyMenu.add(registeredPlayersByHighScoreOption);
		
		menuBar.add(historyMenu); //Adds history menu to menu bar.
		
		//////////////////////////////////////////////////////////
		
		JMenu helpMenu = new JMenu("Help"); //Option containing how-to-use and version/credits.
		helpMenu.setToolTipText("How-to and others");
		
		JMenuItem howToPlayOption = new JMenuItem("How To Use"); //How-to-use instructions.
		howToPlayOption.setToolTipText("Game's instructions");
		howToPlayOption.addActionListener
		(
			new ActionListener() 
			{
				public void actionPerformed(ActionEvent ae) 
				{
					JOptionPane.showMessageDialog(null, "Welcome to The Rolling-Balls game simulator.\n\n"
							+ "This simulator is error proof in the sense that it will "
							+ "not let you perform certain actions if certain requirements\n"
							+ "aren't satisfied. It even take's you trough remedial processes"
							+ "if you happen to fall in one of this situations. But for clarification\n"
							+ "purposes, here's a little information:\n\n"
							+ "Simulating a game:\n"
							+ "On the New Game menu you will find options to create or select players. "
							+ "Trying to select players without them existing will automatically take\n"
							+ "you to the player creation dialog. The files that store all information"
							+ "will be automatically created if they don't exist. You'll have to\n"
							+ "create a username and a password which will be asked upon the selection"
							+ " of your player. Usernames can't be repeated and aren't case sensitive,\n"
							+ "so is the same \"ivan\" as \"IVAN\" When everything is ready, you will "
							+ " be let to simulate a game.\n\n"
							+ "Consulting History:\n"
							+ "In the history option you can see past simulations and registered "
							+ "players in various sorting options. If you want to delete a player\n"
							+ "You will have to go to the games folder and delete the entry from the"
							+ "PlayerList.txt file corresponding to that player. Please watch you don't\n"
							+ "leave a blank line nowhere nor damage other entries, otherwise, the game"
							+ "WILL NOT run. If anything, just delete the files; the program will create\n"
							+ "them again upon exeution. (But you will lose current game history and players!)\n\n"
							+ "Exiting the program:\n"
							+ "Finally, the exit option has another way of closing the program.\n\n"
							+ "Any problems encountered please contact me at: ivan.miranda@upr.edu\n\n"
							+ "Happy simulating!", "Help", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		);
		helpMenu.add(howToPlayOption); 
		
		helpMenu.addSeparator();
		
		JMenuItem versionCreditsOption = new JMenuItem("Version/Credits"); //Option to consult version/credits.
		versionCreditsOption.setToolTipText("Version and dev. info");
		versionCreditsOption.addActionListener
		(
			new ActionListener() 
			{
				public void actionPerformed(ActionEvent ae) 
				{
					JOptionPane.showMessageDialog(null, "Version " + VERSION + "\nDev: "
							+ DEV_NAME + "\nContact: ivan.miranda@upr.edu", "Version", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		);
		helpMenu.add(versionCreditsOption); //Adds option to help menu.
		
		menuBar.add(helpMenu); 
		
		//////////////////////////////////////////////////////////
		
		JMenu exitMenu = new JMenu("Exit"); //Exits program.
		
		JMenuItem closeProgramOption = new JMenuItem("Close Program"); //Tests that user really wants to exit. (curiosity click!)
		closeProgramOption.addActionListener
		(
			new ActionListener() 
			{
				public void actionPerformed(ActionEvent ae) 
				{
					gameWindow.dispose();
				}
			}
		);
		exitMenu.add(closeProgramOption); 
		
		menuBar.add(exitMenu); 
		
		//////////////////////////////////////////////////////////
		
		gameWindow.setJMenuBar(menuBar); 
		gameWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		gameWindow.setResizable(false);
	   	gameWindow.setVisible(true); //Game window complete and visible.
	   	
	} //End launch method.

} //End class.
