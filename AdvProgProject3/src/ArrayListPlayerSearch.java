import java.util.ArrayList;

/**
 * Contains static methods that receive an ArrayList of players and a parameter dependent of each method that search for the corresponding player.
 * @author Ivan J. Miranda
 *
 */
public class ArrayListPlayerSearch
{
	/**
	 * Searches for a player in a given ArrayList of players by its UserName, ignoring case.
	 * @param playerList the ArrayList of players in which the target will be searched. 
	 * @param playerUserName the player's UserName, ignoring case.
	 * @return the Player object that represents the target player. If it is not found, returns null.
	 */
	public static Player searchByUserName(ArrayList<Player> playerList, String playerUserName)
	{
		for (int i = 0; i < playerList.size(); i++)
		{
			if (playerList.get(i).getUserName().equalsIgnoreCase(playerUserName))
			{
				return playerList.get(i);
			}
		}
		
		return null;
	}
	
}
