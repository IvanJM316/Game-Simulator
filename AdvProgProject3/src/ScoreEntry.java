import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Objects of this class represent a RollingBallsGame score entry.
 * @author Ivan J. Miranda
 *
 */
public class ScoreEntry 
{
	private String winnerName;
	private String loserName;
	private int winnerScore;
	private int loserScore;
	private SimpleDateFormat dateOfGameFormat; //Date format wanted for the String representation of the date, as SimpleDateFormat class defines.
	private String dateOfGame;
	
	/**
	 * Creates a ScoreEntry object with the desired parameters and the date format wanted as per the SimpleDateFormat java class. For example,
	 * giving a new SimpleDateFormat object with the String constructor "YYYY/MM/dd-HH:mm:ss" will result in the date February 28, 2015, 
	 * 14:34:22 being formatted and stored as "2015/02/28-14:34:22". The date is retreived from the system at the moment of creation of 
	 * the ScoreEntry object. This constructor should be used to create new score entries and NOT to read entry from files. For that, use the 
	 * other constructor given.
	 * @param winnerName the winner's name.
	 * @param loserName the loser's name.
	 * @param winnerScore the winner's score.
	 * @param loserScore the loser's score.
	 * @param dateOfGameFormat the format wanted for the String representation of the date as per SimpleDateFormat java class. 
	 * Please see SimpleDateFormat java class for further explanation.
	 */
	public ScoreEntry (String winnerName, String loserName, int winnerScore, int loserScore, SimpleDateFormat dateOfGameFormat)
	{
		this.winnerName = winnerName;
		this.loserName = loserName;
		this.winnerScore = winnerScore;
		this.loserScore = loserScore;
		this.dateOfGameFormat = dateOfGameFormat;
		this.dateOfGame = dateOfGameFormat.format(Calendar.getInstance().getTime()); //Gets date and time of creation and converts it to a string,
																					 //given the SimpleDateFormat pattern
	}
	
	/**
	 * Creates a ScoreEntry object with the desired parameters and the already formatted String representation of the date. This constructor 
	 * should be used to read score entries from files.
	 * @param winnerName the winner's name.
	 * @param loserName the loser's name.
	 * @param winnerScore the winner's score.
	 * @param loserScore the loser's score.
	 * @param dateOfGame the String representation of the date, already formatted.
	 */
	public ScoreEntry(String winnerName, String loserName, int winnerScore, int loserScore, String dateOfGame) 
	{
		this.winnerName = winnerName;
		this.loserName = loserName;
		this.winnerScore = winnerScore;
		this.loserScore = loserScore;
		this.dateOfGame = dateOfGame;
	}

	/**
	 * Gets the winner's username for this ScoreEntry.
	 * @return the winner's username.
	 */
	public String getWinnerName() 
	{
		return winnerName;
	}

	/**
	 * Gets the loser's username for this ScoreEntry.
	 * @return the loser's username.
	 */
	public String getLoserName() 
	{
		return loserName;
	}
	
	/**
	 * Gets the winner's score for this ScoreEntry.
	 * @return the winner's score.
	 */
	public int getWinnerScore() 
	{
		return winnerScore;
	}
	
	/**
	 * Gets the loser's score for this ScoreEntry.
	 * @return the loser's score.
	 */
	public int getLoserScore() 
	{
		return loserScore;
	}
	
	/**
	 * Gets the Date object that represents the date of this game, as per the original SimpleDateFormat pattern
	 * @return the Date object that represents the date of this game.
	 */
	public Date getDateOfGame() 
	{
		return dateOfGameFormat.parse(dateOfGame, new ParsePosition(0));
	}
	
	/**
	 * Gets the already formated String representation of this ScoreEntry's date.
	 * @return the formatted String of the date of this ScoreEntry.
	 */
	public String getFormattedDateOfGame()
	{
		return dateOfGame;
	}
	
	/**
	 * Converts this ScoreEntry to its String representation.
	 * @return a String that contains the winner's name, the loser's name, the winner's score, the loser's score and the formatted String
	 * representation of the date of this game, each separated by a space.
	 */
	public String toString()
	{
		return winnerName + " " + loserName + " " + winnerScore + " " + loserScore + " " + dateOfGame;
	}
	
} //End ScoreEntry class.
