import java.util.Comparator;

/**
 * A comparator object that allows you to compare ScoreEntry objects by their date in the specific String 
 * format YYYY/MM/dd-HH:mm:ss. This format can be obtained from a SimpleDateFormat object, from which
 * ScoreEntry objects are constructed form. The secret behind this date format is that the order followed
 * by the String comparison of this formatted date is the same as the lexicographical order of the String
 * alone.
 * @author Ivan J. Miranda
 *
 */
public class ScoreEntryDateComparator implements Comparator<ScoreEntry>
{
	@Override
	/**
	 * Compares two ScoreEntry objects by their date formatted as YYYY/MM/dd-HH:mm:ss, taking latest dates as first.
	 * @return a negative integer if sc1's date of game is after sc2's date of game, 0 if sc1's date of game is the same as sc2's 
	 * date of game, or a positive integer if sc1's date of game is before sc2's date of game.
	 */
	public int compare(ScoreEntry sc1, ScoreEntry sc2) 
	{
		//COMPARES DATES USING COMPARETO METHOD OF STRINGS, AS THE DATE IS ALREADY FORMATTED IN AN ORDER THAT 
		//MAKES THE LEXICOGRAPHIC ORDER THE SAME AS CALENDAR ORDER YYYY/MM/DD-HH:MM:SS (LATEST FIRST)
		return sc2.getFormattedDateOfGame().compareTo(sc1.getFormattedDateOfGame()); 
	}
}
