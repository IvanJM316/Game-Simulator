import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import javax.swing.JPanel;

@SuppressWarnings("serial")
/**
 * This is the title screen of The Rolling Balls Game.
 * @author Ivan J. Miranda
 *
 */
public class TitleScreen extends JPanel
{
	//Some values are a ratio of frame's size.
	public static float titleFontSize = TheRollingBallsGame.gameWindowWidth/20.0f;
	public static float titleX = TheRollingBallsGame.gameWindowWidth/20.0f;
	public static float titleY = TheRollingBallsGame.gameWindowHeight/2.0f - titleFontSize;
	public static final Color BACKGROUND_COLOR = Color.getHSBColor(.5f, .1f, .9f); //Light blue color.
	public static final Color TITLE_COLOR = Color.BLACK;
	public static final Font TITLE_FONT = new Font("SansSerif", Font.BOLD, (int)titleFontSize);
	public static final Font SUBTITLE_FONT = new Font("SansSerif", Font.BOLD, (int)(titleFontSize/1.60f));
	public static final Font PLAYER_INFO_FONT = new Font("SansSerif", Font.BOLD, (int)(titleFontSize/2.5f));
	
	/**
	 * Paints the title screen to the frame. Used by JFrame in order to display an object of this type.
	 */
	public void paintComponent(Graphics g)
	{
		//Paints background color.
		g.setColor(BACKGROUND_COLOR);
		g.fillRect(0, 0, TheRollingBallsGame.gameWindowWidth, TheRollingBallsGame.gameWindowHeight);
		
		//Draws game's title.
		g.setColor(TITLE_COLOR);
		g.setFont(TITLE_FONT);	
		g.drawString(TheRollingBallsGame.GAME_TITLE, (int)titleX, (int)titleY);
		
		//Draws subtitle.
		g.setFont(SUBTITLE_FONT);
		g.drawString("by " + TheRollingBallsGame.DEV_NAME, (int)titleX, (int)(titleY + titleFontSize));
		
		//Draws current player selection.
		g.setFont(PLAYER_INFO_FONT);
		if(PlayerManager.currentPlayer1 == null)
		{
			g.drawString("Player 1: None selected", (int)titleX, (int)(titleY + 2*titleFontSize + 25));	
		}
		else
		{
			g.drawString("Player 1: " + PlayerManager.currentPlayer1.getUserName(), (int)titleX, (int)(titleY + 2*titleFontSize + 25));	
		}
		
		if(PlayerManager.currentPlayer2 == null)
		{
			g.drawString("Player 2: None selected", (int)(titleX + TheRollingBallsGame.gameWindowWidth/3.0f), (int)(titleY + 2*titleFontSize + 25));
		}
		else
		{
			g.drawString("Player 2: " + PlayerManager.currentPlayer2.getUserName(), (int)(titleX + TheRollingBallsGame.gameWindowWidth/3.0f), (int)(titleY + 2*titleFontSize + 25));	
		}
		
	} //End paintComponent method.
	
} //End class.
