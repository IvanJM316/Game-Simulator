import java.util.Comparator;

/**
 * A comparator object that allows you to compare Player objects by their UserName. It follows the natural lexicographical order of strings.
 * @author Ivan J. Miranda
 *
 */
public class PlayerListNameComparator implements Comparator<Player>
{
	@Override
	/**
	 * Compares two Player objects by their usernames.
	 * @return a negative integer if p1's username precedes p2's username, 0 if p1's username is equal to p2's username,
	 * or a positive integer if p1's username goes after p2's username, lexicographically.
	 */
	public int compare(Player p1, Player p2) 
	{
		return p1.getUserName().compareToIgnoreCase(p2.getUserName()); 
	}

}
