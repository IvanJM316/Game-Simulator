import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

/**
 * A static class containing methods that manage the list of score entries and interacts with the user via the programs GUI.
 * @author Ivan J. Miranda
 *
 */
public class ScoreEntryManager 
{
	public static final int MAX_SCORE = 1500; //Max score for the RollingBallsGame.
	
	/**
	 * Simulates a game with the players specified and the SimpleDateFormat wanted to format the String representation of the date of the game.
	 * @param gamesHistory the ArrayList of ScoreEntry objects where the game history is being stored. 
	 * @param p1 player 1.
	 * @param p2 player 2.
	 * @param dateOfGameFormat the SimpleDateFormat wanted for the date of the game. For example,
	 * giving a new SimpleDateFormat object with the String constructor "YYYY/MM/dd-HH:mm:ss" will result in the date February 28, 2015, 
	 * 14:34:22 being formatted and stored as "2015/02/28-14:34:22". The date is retreived from the system at the moment of creation of 
	 * the ScoreEntry object, which is when a game is simulated.
	 */
	public static void simulateGame(ArrayList<ScoreEntry> gamesHistory, Player p1, Player p2, SimpleDateFormat dateOfGameFormat)
	{
		int p1Score, p2Score;
		p1Score = (int) Math.floor(Math.random()*MAX_SCORE + 1); //Random scores between 0 and 1500
		p2Score = (int) Math.floor(Math.random()*MAX_SCORE + 1); //Random scores between 0 and 1500
		//RANDOM SCORES GENERATED.
		
		//P1 WON
		if(p1Score > p2Score)
		{
			gamesHistory.add(new ScoreEntry(p1.getUserName(), p2.getUserName(), p1Score, p2Score, dateOfGameFormat));
			JOptionPane.showMessageDialog(null, "Congrats " + p1.getUserName() + ", you won!" 
					+ " Your score was: " + p1Score + ". Your opponent score was: " + p2Score + ".", "Simulation Results", JOptionPane.INFORMATION_MESSAGE);
		}
		//P2 WON
		if(p2Score > p1Score)
		{
			gamesHistory.add(new ScoreEntry(p2.getUserName(), p1.getUserName(), p2Score, p1Score, dateOfGameFormat));
			JOptionPane.showMessageDialog(null, "Congrats " + p2.getUserName() + ", you won!" 
					+ " Your score was: " + p2Score + ". Your opponent score was: " + p1Score + ".", "Simulation Results", JOptionPane.INFORMATION_MESSAGE);
		}
		//TIE
		if(p1Score == p2Score)
		{
			gamesHistory.add(new ScoreEntry(p1.getUserName(), p2.getUserName(), p1Score, p2Score, dateOfGameFormat));
			JOptionPane.showMessageDialog(null, "Well played. Great battle. The game is a tie."
					+ " P1 score: " + p1Score + ". P2 score: " + p2Score + ".", "Simulation Results", JOptionPane.INFORMATION_MESSAGE);
		}
		
		//CHECKS FOR NEW HIGH SCORES FOR EACH PLAYER.
		if(p1Score > p1.getHighScore())
		{
			JOptionPane.showMessageDialog(null, "Congrats " + p1.getUserName()
					+ ", you established a new personal best!" + " Old score: " + p1.getHighScore() 
					+ ". New score: " + p1Score + ".", "Congrats!", JOptionPane.INFORMATION_MESSAGE);
			p1.setHighScore(p1Score);
			p1.setDateOfHighScore(gamesHistory.get(gamesHistory.size() - 1).getFormattedDateOfGame());
		}
		if(p2Score > p2.getHighScore())
		{
			JOptionPane.showMessageDialog(null, "Congrats " + p2.getUserName()
					+ ", you established a new personal best!" + " Old score: " + p2.getHighScore() 
					+ ". New score: " + p2Score + ".", "Congrats!", JOptionPane.INFORMATION_MESSAGE);
			p2.setHighScore(p2Score);
			p2.setDateOfHighScore(gamesHistory.get(gamesHistory.size() - 1).getFormattedDateOfGame());
		}
		
	} //END SIMULATEGAME METHOD.
	
	/**
	 * Show score entries stored in an ArrayList in a visible list, ordered by the given comparator.
	 * @param gamesHistory the ArrayList of ScoreEntry objects.
	 * @param cmp the comparator that establishes the order relation for the score entries.
	 */
	public static void showHistoryBy(ArrayList<ScoreEntry> gamesHistory, Comparator<ScoreEntry> cmp)
	{
		gamesHistory.sort(cmp); //Sort as per the cmp.
		int gamesHistoryWidth = (int)(TheRollingBallsGame.gameWindowWidth/2.0f);
		int gamesHistoryHeight = (int)(TheRollingBallsGame.gameWindowHeight/3.5f);
		
		String[] scoreEntry = new String[gamesHistory.size() + 2]; //Array of score entries Strings.
		scoreEntry[0] = String.format("%-20s%-20s%-20s%-20s%-20s", "Winner", "Loser", "WinnerScore", "LoserScore", "Date(YYYY/MM/DD-Time)"); //Header row.
		scoreEntry[1] = " "; //blank line (divider).
		for(int i = 0; i < gamesHistory.size(); i++)
		{
			ScoreEntry temp = gamesHistory.get(i);
			scoreEntry[i + 2] = String.format("%-22s%-22s%-22s%-22s%-22s", temp.getWinnerName(), temp.getLoserName(), 
											  temp.getWinnerScore(), temp.getLoserScore(), temp.getFormattedDateOfGame());
		}
		//SCORE ENTRIES WERE SORTED AND EXTRACTED TO THE JLIST
		
		JList<String> graphicalGamesHistory = new JList<>(scoreEntry);
		graphicalGamesHistory.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		graphicalGamesHistory.setFixedCellWidth(gamesHistoryWidth);
		JScrollPane graphicalGamesHistoryScroll = new JScrollPane(graphicalGamesHistory);
		
		//PREPARES FOR THE CREATION OF A NEW FRAME THAT SHOWS THE ORDERED LIST.
		JFrame gamesHistoryWindow = new JFrame("History");
		
		JButton okButton = new JButton("OK");
		okButton.addActionListener(new ActionListener()
		{
			 public void actionPerformed(ActionEvent e)
	            {
	                gamesHistoryWindow.dispose(); //Closes window.
	            } 
		}); 
		
		//ADDS COMPONENTS TO JPANEL, AND IT TO JFRAME.
		JPanel selectionWindow = new JPanel();
		selectionWindow.add(graphicalGamesHistoryScroll, Container.CENTER_ALIGNMENT);
		selectionWindow.add(okButton, Container.BOTTOM_ALIGNMENT);
		selectionWindow.setBackground(Color.WHITE);
		
		gamesHistoryWindow.add(selectionWindow);
		gamesHistoryWindow.setSize(gamesHistoryWidth + 25, gamesHistoryHeight);
		gamesHistoryWindow.setLocation((int)(7f*TheRollingBallsGame.gameWindowWidth/15), (int)(4f*TheRollingBallsGame.gameWindowHeight/10));
		gamesHistoryWindow.setResizable(false);
		gamesHistoryWindow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		gamesHistoryWindow.setVisible(true);	 //FRAME IS CREATED, READY AND VISIBLE	
		
	} //End showHistoryBy method.
	
	/**
	 * Read history from file assuming that each entry is stored as a single line that separates each of its instance variables by a space,
	 * and with NO new lines at the end, and stores them in an ArrayList of ScoreEntry objects, and returns the list.
	 * @param gamesHistoryFile the File from where the score entries will be read.
	 * @return an ArrayList of ScoreEntry objects that contain the score entries stored in file. Returns null if the file didn't exist and could
	 * not be created.
	 */
	public static ArrayList<ScoreEntry> readHistoryFromFile(File gamesHistoryFile) //throws FileNotFoundException
	{
		ArrayList<ScoreEntry> gamesHistory = new ArrayList<ScoreEntry>();
		
		//TODO consider doing a FileUtils generic method that receives a reader operator and a list.
		try {
			Scanner gamesHistoryIn = new Scanner(gamesHistoryFile);
			
			//TODO consider doing a "reader" interface and a playerReader that receives the data file and a games history.
			while(gamesHistoryIn.hasNextLine())
			{
				gamesHistory.add(new ScoreEntry(gamesHistoryIn.next(), gamesHistoryIn.next(), Integer.parseInt(gamesHistoryIn.next()), Integer.parseInt(gamesHistoryIn.next()), gamesHistoryIn.next()));
			}
			//TODO consider doing a "reader" interface and a playerReader that receives the data file and a games history.
			
			gamesHistoryIn.close();
		} catch (FileNotFoundException e) { //FILE WAS NOT FOUND. PROCEEDING TO CREATE IT.
			try {
				new FileWriter(gamesHistoryFile).close(); //TRYING TO CREATE FILE.
			} catch (IOException e1) {
				return null; //FILE COULD NOT BE CREATED. //TODO CONSIDER RETURNING THE EXCEPTION (LIKE WRITETOFILE METHOD AND DOING THE SAME WITH A TRY/CATCH.
			}
			gamesHistory = readHistoryFromFile(gamesHistoryFile); //FILE WAS SUCCEFULY CREATED. WE CALL THE METHOD AGAIN TO RECEIVE THE
															  //PLAYER LIST, EVEN IF IT'S EMPTY.
		}
		//TODO consider doing a FileUtils generic method that receives a reader operator and a list.
		
		return gamesHistory; //HISTORY WAS READ SUCCEFULLY, IF ANY. FILE WAS CREATED IF IT DIDN'T EXIST. (IT DOESN'T EXIST IF IS IS
						   //THE FIRST TIME THE GAME IS EXECUTED BY ANY USER.
	} //End readFromFile method.

	/**
	 * Writes to file the given ArrayList of ScoreEntry objects by their String representation. (toString() method)
	 * @param gamesHistory the ArrayList of ScoreEntry objects to be written to file.
	 * @param gamesHistoryFile the file where the ArrayList of ScoreEntry objects will be written.
	 * @throws IOException if for any reason an error occurs in the writing process.
	 */
	public static void writeToFile(ArrayList<ScoreEntry> gamesHistory, File gamesHistoryFile) throws IOException
	{
		FileWriter gamesHistoryOut = new FileWriter(gamesHistoryFile, false); //PLAYER'S DATABASE OR SAVE FILE.
		for(int i = 0; i < gamesHistory.size(); i++)
		{
			if(i != gamesHistory.size() - 1) //Writes a new line if it is not the last entry.
			{
				gamesHistoryOut.write(gamesHistory.get(i).toString());
				gamesHistoryOut.write(String.format("%n"));
			}
			else
				gamesHistoryOut.write(gamesHistory.get(i).toString());
		}
		gamesHistoryOut.close(); //PLAYER SUCCESFULLY SAVED.
		
	} //End writeToFile method.
	
} //End ScoreEntryManager class.
